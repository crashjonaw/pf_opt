# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 12:56:31 2017

Investment Portfolio Optimization with Python

Method: Lagrange's Multipliers

mu_p = w*mu
var_p = w.T * cov_matrix * w
w.T * 1_n = 1
L = (1.2)var_p - lmbda(w.T*mu - mu_p) - phi(w.T * 1_n - 1)

by setting dL/dw = 0
cov_matrix*w - lmbda(mu) - phi(1_n) = 0

w(*) = inv_cov(lmbda*mu + phi(1_n))

A = mu.T * inv_cov * mu
B = mu.T * inv_cov * 1_n
C = (1_n).T * inv_cov * 1

var_p = (C*mu_p**2 - 2B*mu_p + A)/ (AC-B**2)

"""

import pandas_datareader as pdr                     #to pull data from yahoo,google
from pandas_datareader._utils import RemoteDataError#to use in except when clawing data from data source fails
import numpy as np                                  #to do math
import pandas as pd                                 #to use dataframe, organize data
import matplotlib.pyplot as plt                     #to plot graph
import matplotlib.ticker as ticker                  #to set x and y interval
import datetime as dt                               #to pull the latest date

def optimize(stocks, start_date = '01/01/2010', end_date = dt.date.today().strftime('%m/1/%Y'), num_portfolios = 10000 ): #set default start date to be 01/01/2010, default end date to be the first day of the current month
    stocks = [arg for arg in stocks]                #iterable to store the stocks which user wants to look into
                                                    #this is passed into DataReader to gather stock data
                                                    #stocks = list
    #sort stocknames by letter
    stocks = sorted(stocks)                         #sort the list of stocks to alphabetical order, Data reader requires it to be sorted in alphabetical order
    
    'obtaining data from source'
    daterange = pd.date_range(start=start_date,end=end_date,freq='D')
    data_list=[]
    for stock in stocks:
        attempts = 0                                    #set number of attempts to pull data from data source
        while attempts<3:
            try:
                data = pdr.data.DataReader(stock, data_source = 'morningstar', start = start_date, end = end_date)['Close']     
                data.reset_index(level=0, drop=True,inplace=True)
                data = data.reindex(index=daterange,fill_value = np.nan)
                data.fillna(method='ffill',inplace=True)
                data.fillna(method='bfill',inplace=True)
                data_list.append(data)
                print(data.head())
                break
            
            except RemoteDataError:                                                                             #if RemoteDataError occurs
                attempts += 1                                                                                   #increase number of attempts by one
                print('Data source currently unavailable! Trying to fetch data again. Remaining attempts: %s'%(3-attempts)) #print message
        else:
            print ('Error in fetching data from data source! Please try again!')
            return               
                                                                       # *** escape out the function after 3 attempts! ****
    'convert daily stock price to returns'
    data = pd.DataFrame(dict(zip(stocks,data_list)),index=daterange)
    #print(data.head())
    #data = data.iloc[::-1]                                              #invert data to get the correct returns
    returns = data.pct_change()*100                                     #change the prices to returns in percentage, it is done in relation to the first price, returns = dataframe
                                                                        #we will get back n-1 days of returns
    
    'calculate mean daily return and covariance of daily return'
    mean_daily_returns = returns.mean()                                 #get one number for each stock, which is the mean of the %returns, type = pandas.core.series.Series
    #print(returns.head())
    cov_matrix = returns.cov()                                        #compares each stock given with every other stock and itself, output is each of the comparison's covariance
    print(cov_matrix)
    'obtaining the inverse of the cov_matrix'
    inv_cov = np.array(np.linalg.inv(cov_matrix))                       #using numpy package, linalg, to inverse the cov_matrix to get inv_cov, datastruc = numpy.ndarray


    mu = np.array([[i] for i in mean_daily_returns])                                          
    one_n = np.ones((len(stocks),1))                                    #get column vector with ones for each stock
    
    'Define A ,B ,C'
    A = np.matmul(mu.T,np.matmul(inv_cov, mu))                          #mu.T = transpose of mu
    B = np.matmul(mu.T,np.matmul(inv_cov, one_n))                       #matmul = matrix multiplication
    C = np.matmul((one_n).T, np.matmul(inv_cov,one_n))
    A = A[0][0]                                                         #A = [ [k] ], use A[0][0] to obtain k
    C = C[0][0]
    B = B[0][0]
    
    
    'Monte-Carlo simulation of portfolios'
    #set number of runs of random portfolio weights
     
    #set up array to hold results
    results = np.zeros((3+len(stocks),num_portfolios))                  #creating an array to hold the results; 3 is to store [portfolio return, portfolio std. dev., sharpe]
     
     
    for i in range(num_portfolios):
        weights = np.random.random(len(stocks))                         #select random weights for portfolio holdings
        weights /= np.sum(weights)                                      #rebalance weights to sum to 1
     
        
        portfolio_return = np.sum(mean_daily_returns * weights) * 252                   #calculate portfolio return and volatility
        portfolio_std_dev = np.sqrt(np.dot(weights.T,np.dot(cov_matrix, weights))) * np.sqrt(252)
     
        
                                                                        ### COLUMNS REPRESENTS EACH SIMULATION, ROWS ARE THE INPUT PARAMETERS/RESULTS
        results[0,i] = portfolio_return                                 #store portfolio return in results matrix, row 0 column i 
        results[1,i] = portfolio_std_dev                                #store portfolio return in results matrix, row 1 column i 
        results[2,i] = results[0,i] / results[1,i]                      #store portfolio sharpe in results matrix, row 2 column i
        for j in range(len(weights)):
            results[j+3,i] = weights[j]                                 #storing the simulated random inputs for each weight into (row 3 to row j+3)  column i since there are j number of stocks
    
    
    results_frame = pd.DataFrame(results.T,columns=['ret','stdev','sharpe']+[stocks[i] for i in range(len(stocks))]) #convert transposed results array to Pandas DataFrame with column headers,
                                                                                                                     #returns, stdev, sharpe, *args i.e. the name of each stock in portfolio
    
    
    max_sharpe_port = results_frame.iloc[results_frame['sharpe'].idxmax()]      #locate position of portfolio with highest Sharpe Ratio using the dataframe by find the slicing the data frame using the index with the sharpe being the highest
                                                                                #the index location is found using results_frame['sharpe'].idxmax() which means to find the index where the sharpe is the highest
    min_vol_port = results_frame.iloc[results_frame['stdev'].idxmin()]          #locate position of portfolio with minimum volatility using the dataframe by find the slicing the data frame using the index with the volatility being the lowest
                                                                                #the index location is found using results_frame['stdev'].idxmin() which means to find the index where the volatility is the lowest
     
    #### Plotting the graph ###
    mu_p = np.linspace(-1,1,1000)                                                #setting up a linear space for the y-array, as plotting a graph requires a Y-list and an X-list, in this case we have x = f(y) where x is the annualised volatiltiy and y is the annualised returns of the portfolio
    sd_p = np.sqrt((C*mu_p**2 - 2*B*mu_p + A)/ (A*C-B**2))                      #using lagrange optimisation formula to get the X-list, volatility
    sharpe_p = mu_p*np.sqrt(252)*100/sd_p                                                    #finding the sharpe ratio using annualised volatility and returns
    f, ax = plt.subplots(figsize=(7,5))                                         #define the plot, f represents the name of figure and ax attributes of the plot
    ax.plot(sd_p*np.sqrt(252),mu_p*252)                                         #plot the line for the efficient frontier of the portfolio of returns against volatility
    ax.plot(sharpe_p,mu_p*252)                                                  #plot the line for the portfolio returns against sharpe ratio

    color_plot = ax.scatter(results_frame.stdev,results_frame.ret,c=results_frame.sharpe,cmap='RdYlBu') #plotting the points for each simulation and giving them a color scape according to the value of the sharpe ratio: RdYlBu is Red Yellow Blue
    ax.set_title('Portfolio Analysis', fontsize =15)                            #setting the title of the plot
    ax.set_xlabel('sd_p/sharpe_ratio (%)')                                      #setting the x-axis label of the plot
    ax.set_ylabel('return_p (%)')                                               #setting the y-axis label of the plot
    ax.set_ylim ([-10,100])                                                       #setting the visible y-range of the plot
    ax.set_xlim ([0,max_sharpe_port[2]*120])                                      #setting the visible x-range of the plot
    ax.xaxis.set_major_locator(ticker.MultipleLocator(20))                      #setting the tick size/interval between x-axis labels of the plot
    ax.yaxis.set_major_locator(ticker.MultipleLocator(10))                      #setting the tick size/interval between y-axis labels of the plot
    
    ax.scatter(max_sharpe_port[1],max_sharpe_port[0],marker='x',color='r',s=200)#plot red cross to highlight position of portfolio with highest Sharpe Ratio
    ax.scatter(min_vol_port[1],min_vol_port[0],marker='x',color='g',s=200)      #plot green crpss to highlight position of minimum variance portfolio
    cbar = plt.colorbar(color_plot)                                             #creating the color bar for the plot on the right hand side
    cbar.set_label('sharpe_ratio', rotation=270,labelpad=20)                    #labelling the color bar 
    name = 'portfolio_%s_%s_%s.png'%(str(stocks),start_date.replace('/','-'),str(np.random.random(1)[0]).split('.')[1])
    f.savefig(name,dpi=400) #saving the picture of the figure
    #f.show()                                                                    #showing the plot
    
    
    print(max_sharpe_port)                                                      #extra prints
    print(min_vol_port)                                                         #extra prints
    return f, max_sharpe_port, min_vol_port, [A,B,C], name                           #returns the graph f, data for the portfolio with max sharpe, data for portfolio with min volatility, [A,B,C] for the curve equation
    
    

### Testing of codes 
'AEM - gold'
'AKS - steel and iron'
'GLD - gold etf'
'TSLA - tech'
'UBS - bank'
'NVDA - GPU'
'AMD- advanced micro technology'
'INTC- intel corporation'
'TTM- tata motors'


#f, max_sharpe, min_vol, coeff, name = optimize(['^VIX','^GSPC','AMD','INTC','AMZN','TTM'])

if __name__ == '__main__':
    stocks = ['TSLA','AMD']
    optimize(stocks, start_date = '01/01/2015', end_date ='01/01/2016', num_portfolios = 100 )
























