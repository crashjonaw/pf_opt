# -*- coding: utf-8 -*-
"""
Created on Thu Oct 12 10:54:50 2017

QF205 Project:
    
GUI for portfolio management
- Kelly's portfolio criterion
- Markowitz's portfolio

@author: Aw Jia Yu
"""

import sys
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic
from PyQt5.QtGui import QPixmap
from df_to_gui import PandasModel
from MPT import optimize
from Kelly import kelly

qtCreatorFile = "qf205gui_final.ui"
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class Main(QMainWindow, Ui_MainWindow):          
    def __init__(self):
        super().__init__()
        self.setupUi(self)  
        self.pushButton_calculate.clicked.connect(self.PB_C)
        
        
    def PB_C(self):
        #getting the inputs
        stocks = str(self.lineEdit_stocks.text()).split(',')
        print(stocks)
        start_date = str(self.dateEdit_start.text())
        print(start_date)
        end_date = str(self.dateEdit_end.text())
        print(end_date)
        #risk_free_rate =float(self.lineEdit_rf.text())
        num_portfolios = int(self.lineEdit_n_portfolios.text())
        print(num_portfolios)
        wealth = float(str(self.lineEdit_wealth.text()))
        print(wealth)
        print(self.lineEdit_rf.text())
        if self.lineEdit_rf.text() == ' ':
            rf = ''
            pass
        else:
            rf = float(str(self.lineEdit_rf.text()))
        
        #MPT.py
        f, max_sharpe, min_vol, coeff, figname = optimize(stocks, start_date = start_date, end_date = end_date, num_portfolios = num_portfolios)
        
        #Kelly.py
        df = kelly(stocks,start_date = start_date, end_date = end_date, risk_free_rate = rf )
        
        #max_sharpe table
        max_sharpe = max_sharpe.to_frame().reset_index()
        max_sharpe = max_sharpe.set_index('index',drop=True)
        max_sharpe.columns = ['value']
        max_sharpe['value'][:1]=max_sharpe['value'][:1].round(2)
        max_sharpe['value'][4:]=max_sharpe['value'][4:].round(9)
        max_sharpe['invest ($)'] = max_sharpe['value']*wealth
        max_sharpe['invest ($)'] = max_sharpe['invest ($)'].round(2)
        max_sharpe['invest ($)'][:3]= None
        
        #min_vol table
        print(max_sharpe)
        min_vol = min_vol.to_frame().reset_index()
        min_vol = min_vol.set_index('index',drop=True)
        min_vol.columns = ['value']
        min_vol['value'][:1]=min_vol['value'][:1].round(2)
        min_vol['value'][4:]=min_vol['value'][4:].round(9)
        min_vol['invest ($)'] = min_vol['value']*wealth
        min_vol['invest ($)'] = min_vol['invest ($)'].round(2)
        min_vol['invest ($)'][:3]= None
        print(min_vol)
        ms = PandasModel(max_sharpe)
        self.tableView_msp.setModel(ms)
        mv = PandasModel(min_vol)
        self.tableView_mvp.setModel(mv)
    
        #kelly table
        df['invest ($)'] = df['value']*wealth
        df['invest ($)'] = df['invest ($)'].round(2)
        df['value'] = df['value'].round(5)
        
        leverage = df['invest ($)'].sum() - wealth
        self.textBrowser_lev.setText('$ %s'%leverage)
        self.textBrowser_pamt.setText('$ %s'%(leverage+wealth))
        fkp = PandasModel(df)
        self.tableView_fkp.setModel(fkp)
        
        #plotting the graph
        pixmap = QPixmap('%s'%figname)
        print(figname)
        self.label_graph.setScaledContents(True)
        self.label_graph.setPixmap(pixmap)
        self.label_graph.show()
        print('done')
         
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = Main()
    main.setWindowTitle('Porfolio Management')
    main.show()
    sys.exit(app.exec_())
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        