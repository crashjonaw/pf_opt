# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 15:24:26 2017

Kelly's criterion & applications to the stock market

Objective:
    - maximise the expected growth rate given a portfolio of stocks
    - find out what is the optimal fraction to invest in each asset and the leverage to use in investing
    - investigate the effects of a fractional Kelly approach
    
    
@author: Aw Jia Yu
"""
import pandas as pd
import pandas_datareader as pdr
from pandas_datareader._utils import RemoteDataError #to use in except when clawing data from data source fails
import numpy as np                                   #to do math
import datetime as dt                                #to pull the latest date

def kelly(stocks, start_date = '01/01/2010', end_date = dt.date.today().strftime('%m/%d/%Y'), risk_free_rate = ''):
    returns = None
    #sort stocknames by letter
    stocks = sorted(stocks)                         #sort the list of stocks to alphabetical order, Data reader requires it to be sorted in alphabetical order
    
    'obtaining data from source'
    daterange = pd.date_range(start=start_date,end=end_date,freq='D')
    data_list=[]
    for stock in stocks:
        attempts = 0                                    #set number of attempts to pull data from data source
        while attempts<3:
            try:
                data = pdr.data.DataReader(stock, data_source = 'morningstar', start = start_date, end = end_date)['Close']     
                data.reset_index(level=0, drop=True,inplace=True)
                data = data.reindex(index=daterange,fill_value = np.nan)
                data.fillna(method='ffill',inplace=True)
                data.fillna(method='bfill',inplace=True)
                data_list.append(data)
                print(data.head())
                break
            
            except RemoteDataError:                                                                             #if RemoteDataError occurs
                attempts += 1                                                                                   #increase number of attempts by one
                print('Data source currently unavailable! Trying to fetch data again. Remaining attempts: %s'%(3-attempts)) #print message
        else:
            print ('Error in fetching data from data source! Please try again!')
            return               
                                                                       # *** escape out the function after 3 attempts! ****
    'convert daily stock price to returns'
    data = pd.DataFrame(dict(zip(stocks,data_list)),index=daterange)    
    
    #currently doesn't work as yahoo finance is gone!!!
    '''
    'obtaining the risk free rate'
    
    if risk_free_rate == '':
        attempts=0
        while attempts<3:
            try:
                today_date = dt.date.today().strftime('%m/%d/%Y')
                T_note = pdr.data.DataReader('^TNX', data_source  ='morningstar', start = today_date)['Close']
                risk_free_rate = list(T_note)[0]/10 #convert % into interest rate terms
                print (risk_free_rate)
                break
            except RemoteDataError:                                                                             #if RemoteDataError occurs
                attempts += 1                                                                                   #increase number of attempts by one
                print('Data source currently unavailable! Trying to fetch data again. Remaining attempts: %s'%(3-attempts)) #print message
       
    else:
        pass
    '''
    
    'getting expected return'
    returns = data.pct_change()                             #getting percentage return
    mdr = returns.mean()*100
    cov_matrix = returns.cov()*100
    print(mdr)
    
    'Algorithm'
    'growth rate = E[ln( (1+r) + sum(U_k(r_k-r)) )]'
    'U = (1 + r) (∑^-1) (R - ONE*r)'
    r = risk_free_rate/252
    'Finding ∑^-1 i.e. second moments about the risk-free rate'
    ncm = np.zeros((len(stocks),len(stocks)))
    for i in range(len(ncm)):
        ncm[i] = [cov_matrix.iloc[i][j]+mdr[i]*mdr[j]-r*(mdr[i] + mdr[j]) +r**2 for j in range(len(stocks))]    #forming the covariance matrix about the borrowing rate
    ncm = np.matrix.round(ncm,3)
    try:
        ncm_inverse = np.linalg.solve(ncm,np.identity(len(stocks)))
        ncm_inverse = np.matrix.round(ncm_inverse,3)
        U = np.matrix.round((1+r/100)*np.matmul(ncm_inverse,np.array(mdr-r).reshape(len(stocks),1)),2) #E[(r_i-rf)(r_j-rf)] = E(r_i x r_f) - rf(E(r_i)+E(r_j)) + r_f^2 
                                                                                                        #= Cov(r_i,r_j) + E(r_i)E(r_j) - rf(E(r_i)+E(r_j)) + r_f^2 
        weights = [U[i][0] for i in range(len(stocks))]
    except:
        weights = [0 for i in range(len(stocks))]
    df = mdr.to_frame().reset_index()
    df = df.set_index('index',drop=True)
    df.columns = ['value']
    df['value']= weights
    return df
    
    
#df = kelly(['^VIX','AMD','INTC','AMZN','TTM'], start_date = '01/01/2012', end_date = dt.date.today().strftime('%m/1/%Y'), risk_free_rate = '')
#print(df)
    
    
    
    
    
    
    
    